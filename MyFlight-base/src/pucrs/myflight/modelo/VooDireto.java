package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;

public class VooDireto extends Voo {
	
	private Rota rota;

	public VooDireto(LocalDateTime datahora, Rota rota) {
		super(datahora);
		this.rota = rota;
	}

	@Override
	public Rota getRota() {
		return rota;
	}

	@Override
	public Duration getDuracao() {
		// Calcular duracao em funcao da distancia
		
		double dist = Geo.distancia(rota.getOrigem().getLocal(), rota.getDestino().getLocal());
		double hour = (dist/805)+0.5;
		
		return Duration.ofMinutes((long)hour);
	}

}
